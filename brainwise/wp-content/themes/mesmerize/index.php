<?php mesmerize_get_header(); ?>
<div>
    <div class="row" style=" width: 80%; background: white; margin-top: -50px; margin-left: 10%; z-index: 10; position: absolute;"> 
                 <div class="col-sm-4">
                      <img src="https://www.docustream.com.au/assets/images/how.png" class="icon-newspaper">
                            <h5>How It Works</h5>
                            <p style="font-style: italic;"> 
Using DocuStream, you can generate contracts using our powerful document generation technology based on templates created by Arnotts Technology Lawyers, trusted legal advisors to the Australian technology and telecommunications industries. </p>
                 </div> 
                   <div class="col-sm-4">
                     <img src="https://www.docustream.com.au/assets/images/query.jpg" class="icon-newspaper">
                            <h5>Need help selecting <br>a template?</h5>
                            <p style="font-style: italic;"> 
If you are uncertain about your requirements, feel free to call us. We will help you choose the appropriate document.</p>
                            <a href="tel: (02) 8238-6989" class="mt_btn_grey" style="border:1px solid #215A66;background-color: transparent;color: #215A66;"> <i class="ion-ios-telephone-outline"></i>&nbsp;&nbsp; (02) 8238-6989</a>
                 </div> 
                 <div class="col-sm-4">
                    <img src="https://www.docustream.com.au/assets/images/logoa.png" class="icon-newspaper">
                            <h5>Need a fully tailored legal document?</h5>
                            <p style="font-style: italic;"> 
Arnotts Technology Lawyers can prepare a fully customised legal contract based on your specific requirements </p>
                 </div> 
            </div>
    </div>
<style>
html{
        margin-top: 0px !important;
}
</style>
    <div class="content blog-page" style="padding-top:30%;">
        <div class="gridContainer <?php mesmerize_page_content_wrapper_class(); ?>">
            <div class="row">
                <div class="col-xs-12 <?php mesmerize_posts_wrapper_class(); ?>">
                    <div class="post-list row" <?php mesmerize_print_blog_list_attrs(); ?>>
                        <?php
                        if (have_posts()):
                            while (have_posts()):
                                the_post();
                                get_template_part('template-parts/content', get_post_format());
                            endwhile;
                        else:
                            get_template_part('template-parts/content', 'none');
                        endif;
                        ?>
                    </div>
                    <div style="    background: #cadbe6;">
                
                    <div class="overlay" style="padding-top: 10%; padding-bottom: 10%; text-align: center;vertical-align: middle; line-height: 90px;">
<div class="container">
            <div class="row">
                <div class="watch_content">
                    <h1>Brainwise is used by tech startups, SMEs and ASX<br> listed companies</h1>
                    
                </div>
            </div>
        </div>
    </div>
                            </div>
                    <div class="navigation-c">
                        <?php
                        if (have_posts()):
                            mesmerize_print_pagination();
                        endif;
                        ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

<?php get_footer();
