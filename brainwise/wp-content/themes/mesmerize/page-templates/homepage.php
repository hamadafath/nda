<?php
/*
 * Template Name: Front Page Template
 */
mesmerize_get_header('homepage');
?>
    <style>
    html{
        margin-top: 0px !important;
}
    </style>
    <div class="page-content">
        
        <div class="<?php mesmerize_page_content_wrapper_class(); ?>">
            <?php
            Echo do_shortcode ("[slide-anything id='826']");
            while (have_posts()) : the_post();
                the_content();
            endwhile;
            ?>
        </div>
    </div>

<?php get_footer(); ?>
