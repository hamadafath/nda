    <style>
    html{
        margin-top: 0px !important;
}
    </style><div class="navigation-bar <?php esc_attr(mesmerize_header_main_class()) ?>" <?php mesmerize_navigation_sticky_attrs() ?>>
    <div class="navigation-wrapper <?php esc_attr(mesmerize_navigation_wrapper_class()) ?>">
    	<div class="row basis-auto">
	        <div class="logo_col col-xs col-sm-fit">
	            <?php mesmerize_print_logo(); ?>
	        </div>
	        <div class="main_menu_col col-xs">
	            <?php mesmerize_print_primary_menu(); ?>
                <?php
		// add login/logout links
		if(is_user_logged_in()) {
		echo '<a class="log button" href="'. get_site_url().'/wp-login.php?action=logout " >logout</a>';
		}else{
		echo '<a class="log button" href="'. get_site_url().'/wp-login.php " >login/register</a>';

		}
		?>		
	        </div>
	    </div>
    </div>
</div>
